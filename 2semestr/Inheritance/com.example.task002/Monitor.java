package Monitor;

public class Monitor {
	int size,width,height;
	String resolution;
	
	Monitor(int size,String resolution,int width,int height){
		this.size=size;
		this.resolution=resolution;
		this.width=width;
		this.height=height;
	}
	
	Monitor(int size,int width,int height){
		this.size=size;
		this.width=width;
		this.height=height;
		this.resolution=width+"x"+height;
	}
	
	void setSize(int size){
		this.size=size;
	}
	
	int getSize(){
		return size;
	}
	
	void setResolution(String resolution){
		this.resolution=resolution;
	}
	
	String getResolution(){
		return resolution;
	}
	
	void setWidth(int width){
		this.width=width;
	}
	
	int getWidth(){
		return width;
	}
	
	void setHeight(int height){
		this.height=height;
	}
	
	int getHeight(){
		return height;
	}
	
}
