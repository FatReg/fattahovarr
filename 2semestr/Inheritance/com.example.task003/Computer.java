package Computer;

public class Computer {
	int core,freak,ram,harddisk;
	String videocard;
	boolean on=false;
	
	Computer(int core,int freak,int ram,int harddisk,String videocard){
		this.core=core;
		this.freak=freak;
		this.ram=ram;
		this.harddisk=harddisk;
		this.videocard=videocard;
	}
	
	void on(){
		on=true;
		System.out.println("The computer turns on");
	}
	
	void off(){
		on=false;
		System.out.println("Computer shuts down");
	}
	
	int getCore(){
		return core;
	}
	
	void setCore(int core){
		this.core=core;
	}
	
	int getFreak(){
		return freak;
	}
	
	void setFreak(int freak){
		this.freak=freak;
	}
	
	int getRAM(){
		return ram;
	}
	
	void setRAM(int ram){
		this.ram=ram;
	}
	
	int getHarddisk(){
		return harddisk;
	}
	
	void setHarddisk(int harddisk){
		this.harddisk=harddisk;
	}
	
	String getVideocard(){
		return videocard;
	}
	
	void setVideocard(String videocard){
		this.videocard=videocard;
	}
}
