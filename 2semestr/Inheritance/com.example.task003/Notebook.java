package Computer;

class Notebook extends Computer {
	int battery;
	int size,width,height;
	String resolution;
	Notebook(int core,int freak,int ram,int harddisk,String vidocard,int battery){
		super(core, freak, ram, harddisk, vidocard);
		this.battery=battery;
	}
	
	void Monitor(int size,String resolution,int width,int height){
		this.size=size;
		this.resolution=resolution;
		this.width=width;
		this.height=height;
	}
	
	void setSize(int size){
		this.size=size;
	}
	
	int getSize(){
		return size;
	}
	
	void setResolution(String resolution){
		this.resolution=resolution;
	}
	
	String getResolution(){
		return resolution;
	}
	
	void setWidth(int width){
		this.width=width;
	}
	
	int getWidth(){
		return width;
	}
	
	void setHeight(int height){
		this.height=height;
	}
	
	int getHeight(){
		return height;
	}
	
	void setBattery(int battery){
		this.battery=battery;
	}
	
	int getBattery(){
		return battery;
	}
	
	void BatteryPower(){
		System.out.println("Notebook charging");
	}
}
