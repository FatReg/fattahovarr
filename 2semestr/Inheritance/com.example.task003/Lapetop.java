package Computer;

class Lapetop extends Notebook {
	String os;
	
	Lapetop(int core,int freak,int ram,int harddisk,String vidocard,int battery,String os){
		super(core, freak, ram, harddisk, vidocard, battery);
		this.os=os;
	}
	
	void setOS(String os){
		this.os=os;
	}
	
	String getOS(){
		return os;
	}
	
}
