package Transport;

class Bus extends Transport {
	int capacity,man;
	
	Bus(int capacity){
		this.capacity=capacity;
	}
	
	void EnterPeople(int a){
		if(man+a>=capacity){
			System.out.println((capacity-man)+" mans entered. Sorry, bus is full. Number of people= "+capacity);
			man=capacity;
		}
		else{
			man=man+a;
			System.out.println(a+" mans entered. Number of people= "+man);
		}
	}
	
	void OutPeople(int a){
		if(man-a>=0){
			man=0;
			System.out.println("Bus is empty");
		}
		else{
			man=man-a;
			System.out.println(a+" mans went out. Number of people= "+man);
		}
	}
}
