package Transport;

class Car extends Transport{
	boolean trunk;
	
	Car(boolean trunk){
		this.trunk=trunk;
	}
	
	boolean isTrunk(){
		return trunk;
	}
}
