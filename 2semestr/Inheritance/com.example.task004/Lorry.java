package Transport;

class Lorry extends Transport {
int capacity,cargo;
	
	Lorry(int capacity){
		this.capacity=capacity;
	}
	
	void LoadCargo(int a){
		if(cargo+a>=capacity){
			System.out.println((capacity-cargo)+" cargo loaded. Sorry, lorry is full. Number of cargo= "+capacity);
			cargo=capacity;
		}
		else{
			cargo=cargo+a;
			System.out.println(a+" cargo loaded. Number of cargo= "+cargo);
		}
	}
	
	void UnloadCargo(int a){
		if(cargo-a>=0){
			cargo=0;
			System.out.println("Lorry is empty");
		}
		else{
			cargo=cargo-a;
			System.out.println(a+" cargo unloaded. Number of cargo= "+cargo);
		}
	}
}
