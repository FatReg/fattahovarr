import java.util.Random;

public class Game {
	Gird g = new Gird();
	int mas[][] = new int[9][9];
	Random r = new Random();
	int difficulte;

	Game(int difficulte) {
		boolean x = false;
		this.difficulte = difficulte;
		while (!x) {
			newMas();
			x = sudokuField();
		}
	}

	void newMas() { // ����� ������ �� ���� gird
		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas.length; j++)
				mas[i][j] = g.gird[i][j];
	}

	boolean sudokuField() {
		int i, j, k = 0, l = 0;
		while (k < difficulte) {
			i = r.nextInt(9);
			j = r.nextInt(9);
			if (mas[i][j] != 0) {
				int number = mas[i][j];
				mas[i][j] = 0;
				Solution s = new Solution(mas);// ������� ������� � �������
												// �����������
				if (s.hasSolution()) {
					k++;
					l = 0; // ��������� ��������
				} else {
					mas[i][j] = number;
					l++; // ������� ������������
				}
			}
			// ���� ����������� ����� �� ������
			if (l > 40)
				return false;
		}
		return true;
	}
}
