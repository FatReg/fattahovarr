import java.util.HashSet;

public class Solution {
	boolean solution = true;
	int mas[][] = new int[9][9];

	// ����� ������ = ������� �� game
	public Solution(int mas[][]) {
		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas.length; j++)
				this.mas[i][j] = mas[i][j];
	}

	// �������� �������
	boolean hasSolution() {
		filling();
		return !isEmpty();
	}

	// ����� ������� ������ �������
	void filling() {
		boolean x = false;
		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas.length; j++) {
				if (mas[i][j] == 0) {
					if (search(i, j) == true) // ���� ���� ��� ����� ���
												// ����������, ��������� �����
						x = true;
				}
			}
		if (x)
			filling();
	}

	// �������� ������� �� �������������
	boolean isEmpty() {
		boolean x = true;
		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas.length; j++)
				if (mas[i][j] == 0)
					x = true;
				else
					x = false;
		return x;
	}

	// ������� ����� � ���������� ������������ �������(true ���� �����, false
	// ���� ���)
	boolean search(int i, int j) {
		boolean founded = false;
		HashSet<Integer> set = new HashSet<Integer>();
		for (int k = 1; k < 10; k++)
			set.add(k);
		// ��������� �� ����������� � ���������
		for (int k = 0; k < mas.length; k++) {
			set.remove(mas[k][j]);
			set.remove(mas[i][k]);
		}
		// ��������� � ��������� 3�3
		if (i <= 2)
			for (int k = 0; k <= 2; k++)
				if (j <= 2)
					for (int l = 0; l <= 2; l++)
						set.remove(mas[k][l]);
				else if (j <= 5)
					for (int l = 3; l <= 5; l++)
						set.remove(mas[k][l]);
				else
					for (int l = 6; l <= 8; l++)
						set.remove(mas[k][l]);
		else if (i <= 5)
			for (int k = 3; k <= 5; k++)
				if (j <= 2)
					for (int l = 0; l <= 2; l++)
						set.remove(mas[k][l]);
				else if (j <= 5)
					for (int l = 3; l <= 5; l++)
						set.remove(mas[k][l]);
				else
					for (int l = 6; l <= 8; l++)
						set.remove(mas[k][l]);
		else
			for (int k = 6; k <= 8; k++)
				if (j <= 2)
					for (int l = 0; l <= 2; l++)
						set.remove(mas[k][l]);
				else if (j <= 5)
					for (int l = 3; l <= 5; l++)
						set.remove(mas[k][l]);
				else
					for (int l = 6; l <= 8; l++)
						set.remove(mas[k][l]);
		// ���� ������� ����������� ��������� �������, ���������� � ������
		if (set.size() == 1) {
			Integer a[] = new Integer[1];
			set.toArray(a);
			mas[i][j] = a[0];
			founded = true;
		}
		return founded;
	}
}
