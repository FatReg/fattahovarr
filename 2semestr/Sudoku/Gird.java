import java.util.Random;

public class Gird extends Base {
	// ������� �����(���� ���� ����) ����� �����������(���������� �
	// ������������� ������� �����)
	Base b = new Base();
	int gird[][] = b.base;

	Gird() {
		Random r = new Random();
		int k;
		for (int i = 0; i < 20; i++) { // 20 ��� � ��������� ������� �������
										// ������ ����������� �������
			k = r.nextInt(5);
			if (k == 0)
				transposition();
			else if (k == 1)
				swapRowsSmall();
			else if (k == 2)
				swapColumsSmall();
			else if (k == 3)
				swapRowsArea();
			else
				swapColumsArea();
		}
	}

	// ������ �����������

	// ��������������
	void transposition() {
		int a[][] = new int[9][9];

		for (int i = 0; i < a.length; i++)
			for (int j = 0; j < a.length; j++)
				a[i][j] = gird[j][i];
		gird = a;
	}

	// ����� ���� ����� � �������� ������ ������(����� 3 ������ �� 3 ������ �
	// ������)
	void swapRowsSmall() {
		int mas[] = new int[9];
		Random r = new Random();
		int k = r.nextInt(3); // ��������� ���������� ������
		int i = r.nextInt(3); // ��������� ��������� ������� � ������
		int j = r.nextInt(3);
		while (i == j)
			j = r.nextInt(3);// ������� ������ ���� �������
		mas = gird[i + k * 3]; // ������������ ������� ��� ������ �� ������
		gird[i + k * 3] = gird[j + k * 3];
		gird[j + k * 3] = mas;
	}

	// ����� ���� �������� � �������� ������ ������
	void swapColumsSmall() {
		transposition();
		swapRowsSmall();
		transposition();
	}

	// ����� ���� ������� �� �����������
	void swapRowsArea() {
		int mas[][] = new int[3][9];
		Random r = new Random();
		int k = r.nextInt(3);
		int l = r.nextInt(3);
		while (k == l)
			l = r.nextInt(3);
		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas[i].length; j++) {
				mas[i][j] = gird[i + 3 * k][j];
				gird[i + 3 * k][j] = gird[i + 3 * l][j];
				gird[i + 3 * l][j] = mas[i][j];
			}
	}

	// ����� ���� ������� �� ���������
	void swapColumsArea() {
		transposition();
		swapRowsArea();
		transposition();
	}
}
