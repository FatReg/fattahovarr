import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class TitleWindow extends JFrame {

	int mas[][] = new int[9][9];
	int dif;
	JComboBox[][] buttons = new JComboBox[9][9];

	TitleWindow(boolean freeOnClose) {
		super("Sudoku");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// ������� � �������� ���������
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new FlowLayout());
		JLabel label = new JLabel(new ImageIcon("Sudoku.jpg"));
		panel.add(label);

		JButton easy = new JButton("�����");
		ActionListener easyActionListener = new easyActionListener();
		easy.addActionListener(easyActionListener);
		panel.add(easy);

		JButton harder = new JButton("������");
		ActionListener harderActionListener = new harderActionListener();
		harder.addActionListener(harderActionListener);
		panel.add(harder);

		JButton hardest = new JButton("������");
		ActionListener hardestActionListener = new hardestActionListener();
		hardest.addActionListener(hardestActionListener);
		panel.add(hardest);

		setSize(450, 400);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public class easyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			game(50);
		}
	}

	public class harderActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			game(55);
		}
	}

	public class hardestActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			game(60);
		}
	}

	void game(int dif) {
		this.dif = dif;
		// ������ ����
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("����");
		JMenuItem novii = new JMenuItem("�����");
		ActionListener novactionListener = new NovActionListener();
		novii.addActionListener(novactionListener);
		fileMenu.add(novii);
		JMenuItem proverka = new JMenuItem("���������");
		ActionListener proverkaListener = new ProvActionListener();
		proverka.addActionListener(proverkaListener);
		fileMenu.add(proverka);
		menuBar.add(fileMenu);
		setJMenuBar(menuBar);
		gird();// �������� ������
	}

	// �������� ����� ����
	public class NovActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			gird();
		}
	}

	// �������� �������
	public class ProvActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int m[][] = new int[9][9];

			for (int i = 0; i < mas.length; i++)
				for (int j = 0; j < mas.length; j++) {
					m[i][j] = mas[i][j];
					if (mas[i][j] == 0)
						m[i][j] = buttons[i][j].getSelectedIndex();

				}

			for (int i = 0; i < m.length; i++)
				for (int j = 0; j < m.length; j++)
					if (m[i][j] == 0) {
						JOptionPane.showMessageDialog(null,
								"������� �� ���������");
						return;
					}

			for (int i = 0; i < m.length; i++)
				for (int j = 0; j < m.length; j++) {
					for (int l = 0; l < 9; l++) {
						if (m[i][j] == m[l][j]) {
							JOptionPane.showMessageDialog(null,
									"������ �������");
							return;
						}
						if (m[i][j] == m[i][l]) {
							JOptionPane.showMessageDialog(null,
									"������ �������");
							return;
						}
						if (i < 3)
							for (int k = 0; k < 3; k++)
								if (j < 3)
									for (int n = 0; n < 3; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
								else if (j < 6)
									for (int n = 3; n < 6; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
								else
									for (int n = 6; n < 9; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
						else if (i < 6)
							for (int k = 3; k < 6; k++)
								if (j < 3)
									for (int n = 0; n < 3; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
								else if (j < 6)
									for (int n = 3; n < 6; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
								else
									for (int n = 6; n < 9; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
						else
							for (int k = 6; k < 9; k++)
								if (j < 3)
									for (int n = 0; n < 3; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
								else if (j < 6)
									for (int n = 3; n < 6; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
								else
									for (int n = 6; n < 9; n++) {
										if (m[i][j] == m[k][n]) {
											JOptionPane.showMessageDialog(null,
													"������ �������");
											return;
										}
									}
						JOptionPane.showMessageDialog(null, "�����������!");
					}
				}
			// for(int i=0;i<m.length;i++){
			// System.out.println();
			// for(int j=0;j<m.length;j++)
			// System.out.print(m[i][j]);
			// }
		}
	}

	void gird() {

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(9, 9, 1, 1));
		newMas(dif);// ���� ��� ������

		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas.length; j++) {
				panel.add(createButton(i, j));
			}
		setContentPane(panel);

		pack();
		setSize(580, 500);
		setLocationRelativeTo(null);
	}

	void newMas(int dif) {
		Game g = new Game(dif);
		for (int i = 0; i < mas.length; i++)
			for (int j = 0; j < mas.length; j++)
				mas[i][j] = g.mas[i][j];
	}

	private JPanel createButton(int i, int j) {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		if (mas[i][j] != 0) {
			JButton button = new JButton("" + mas[i][j]);
			panel.add(button);

		} else {

			String elements[] = { "", "1", "2", "3", "4", "5", "6", "7", "8",
					"9" };
			JComboBox combo = new JComboBox(elements);
			buttons[i][j] = combo;

			panel.add(combo);

		}
		return panel;
	}
}
