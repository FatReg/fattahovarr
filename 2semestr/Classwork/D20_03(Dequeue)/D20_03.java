package D20_03;

public class D20_03 {

	public class Node {
		int number;
		Node next;
		Node prev;
	}

	private Node header = null;
	private Node end = null;

	void addAtTheBegin(int number) {
		Node node = new Node();
		node.number = number;
		node.prev = null;
		node.next = null;
		if (header == null) {
			header = node;
			end = node;
		} else {
			node.next = header;
			header.prev = node;
			header = node;
		}
	}

	void addAtTheEnd(int number) {
		Node node = new Node();
		node.number = number;
		node.next = null;
		node.prev = null;
		if (header == null) {
			header = node;
			end = node;
		} else {
			end.next = node;
			node.prev = end;
			end = node;
		}
	}

	int removeBegin() {
		if (header == null) {
			System.out.println("������ ������");
			return 0;
		} else {
			int num = header.number;
			if (header == end) {
				end = null;
				header = null;
			} else {
				header = header.next;
				header.prev = null;
			}
			return num;
		}
	}

	int removeEnd() {
		if (header == null) {
			System.out.println("������ ����");
			return 0;
		} else {
			int num = end.number;
			if (header != end) {
				end = end.prev;
				end.next = null;
			} else {
				end = null;
				header = null;
			}
			return num;
		}
	}

	void addBefore(int a, int number) {
		if (header == null)
			return;
		else {
			Node temp = header;
			while (temp.next != null) {
				if (temp == header) {
					if (header.number == a)
						addAtTheBegin(number);
					temp = temp.next;
				}
				if (temp.next.number == a) {
					Node node = new Node();
					node.number = number;
					node.prev = temp;
					node.next = temp.next;
					temp.next.prev = node;
					temp.next = node;
					temp = node.next;
				} else
					temp = temp.next;
			}
		}
	}

	void addAfter(int a, int number) {
		if (header == null)
			return;
		else {
			Node temp = header;
			while (temp != null)
				if (temp.number == a)
					if (temp.next != null) {
						Node node = new Node();
						node.number = number;
						node.prev = temp;
						node.next = temp.next;
						temp.next.prev = node;
						temp.next = node;
						temp = node.next;
					} else {
						addAtTheEnd(number);
						return;
					}
				else
					temp = temp.next;
		}
	}

	void removeBefore(int a) {
		if (header == null)
			return;
		else {
			Node temp = header;
			while (temp.next != null) {
				if (temp == header) {
					if (temp.next.number == a)
						removeBegin();
					temp = temp.next;
				}
				if (temp.next.number == a) {
					temp.prev.next = temp.next;
					temp.next.prev = temp.prev;
					temp = temp.next;
				} else
					temp = temp.next;
			}
		}
	}

	void removeAfter(int a) {
		if (header == null)
			return;
		else {
			Node temp = header;
			while (temp.next != null)
				if (temp.number == a)
					if (temp.next.next != null) {
						temp.next = temp.next.next;
						temp.next.prev = temp;
						temp = temp.next;
					} else {
						removeEnd();
						return;
					}
				else
					temp = temp.next;
		}
	}

	void show() {
		Node temp = header;
		while (temp != null) {
			System.out.print(temp.number + " ");
			temp = temp.next;
		}
	}
}
