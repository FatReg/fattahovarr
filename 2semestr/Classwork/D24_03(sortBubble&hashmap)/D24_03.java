package D24_03;

public class D24_03 {

	public class Node {
		int number;
		Node next;
		Node prev;
	}

	private Node header = null;
	private Node end = null;

	void addAtTheEnd(int number) {
		Node node = new Node();
		node.number = number;
		node.next = null;
		node.prev = null;
		if (header == null) {
			header = node;
			end = node;
		} else {
			end.next = node;
			node.prev = end;
			end = node;
		}
	}

	void sortBubble() {
		long t1 = System.currentTimeMillis();
		if (header == null) {
			System.out.println("������ ����");
		} else {
			Node temp1 = header;
			Node temp = temp1;
			while (temp1 != null) {
				temp = temp1;
				int min = temp.number;
				while (temp != null) {
					if (temp.number < min) {
						int a = min;
						min = temp.number;
						temp.number = a;
					}
					temp = temp.next;
				}
				temp1.number = min;
				temp1 = temp1.next;
			}
		}
		long t2 = System.currentTimeMillis();
		System.out.println("result=" + (t2 - t1));
	}

	void sortBubbleNode() {
		long t1 = System.currentTimeMillis();
		if (header == null)
			System.out.println("������ ����");
		else {
			Node temp1 = header;
			Node temp;
			int min;
			while (temp1 != null) {
				temp = temp1;
				min = temp.number;
				while (temp != null) {
					if (temp.number < min) {
						min = temp.number;
					}
					temp = temp.next;
				}
				temp = temp1;
				while (temp.number != min) {
					temp = temp.next;
				}
				if (temp != temp1) {
					Node temp2 = new Node();
					temp2.number = temp.number;
					addBefore(temp1, temp2);
					remove(temp);
					temp1 = temp1.prev;
				}
				temp1 = temp1.next;
			}
		}
		long t2 = System.currentTimeMillis();
		System.out.println("result=" + (t2 - t1));
	}

	void remove(Node node) {
		if (header == null) {
			System.out.println("������ ����");
		} else {
			if (header == end) {
				header = null;
				end = null;
			} else {
				if (node == header) {
					header = header.next;
					header.prev = null;
				} else {
					if (node == end) {
						end = end.prev;
						end.next = null;
					} else {
						node.prev.next = node.next;
						node.next.prev = node.prev;
					}
				}
			}
		}
	}

	void addBefore(Node mesto, Node node) {
		if (mesto == header) {
			mesto.prev = node;
			node.next = mesto;
			node.prev = null;
			header = node;
		} else {
			node.prev = mesto.prev;
			node.next = mesto;
			mesto.prev.next = node;
			mesto.prev = node;
		}
	}

	void show() {
		Node temp = header;
		while (temp != null) {
			System.out.print(temp.number + " ");
			temp = temp.next;
		}
	}
}
