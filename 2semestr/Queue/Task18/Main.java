package Task18;

import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static Queue<Patient> pattick = new LinkedList<Patient>();
	static Queue<Patient> pattickless = new LinkedList<Patient>();
	static boolean q = true;

	public static void camePatientWithTicket(String name) {
		Patient p = new Patient(name);
		p.ticket(true);
		pattick.add(p);
	}

	public static void camePatientWithoutTicket(String name) {
		Patient p = new Patient(name);
		pattickless.add(p);
	}

	public static void doctorIsFree() {
		if (pattick.peek() != null && pattickless.peek() != null)
			if (q) {
				Patient p = pattick.poll();
				System.out.println(p.name + p.isTicket() + " ����� � �����");
				q = false;
			} else {
				Patient p = pattickless.poll();
				System.out.println(p.name + p.isTicket() + " ����� � �����");
				q = true;
			}
		else {
			if (pattick.peek() == null && pattickless.peek() == null) {
				System.out.println("������� �����");
			} else if (pattick.peek() == null) {
				Patient p = pattickless.poll();
				System.out.println(p.name + p.isTicket() + " ����� � �����");
			} else {
				Patient p = pattick.poll();
				System.out.println(p.name + p.isTicket() + " ����� � �����");
			}
		}
	}

	public static void main(String[] args) {
		camePatientWithTicket("Ted");
		camePatientWithTicket("Sergey");
		camePatientWithoutTicket("Alex");
		doctorIsFree();
		doctorIsFree();
		camePatientWithTicket("Ann");
		doctorIsFree();
		doctorIsFree();
		doctorIsFree();
	}
}
