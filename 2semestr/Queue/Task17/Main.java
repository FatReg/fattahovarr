package Task17;

import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static Queue<Student> students=new LinkedList<Student>();
	
	public static void addStudent(String name,String surname,String patronymic,String faculty,String group){
		Student s=new Student(name, surname, patronymic, faculty, group);
		students.add(s);
		System.out.println(name+" "+patronymic+" "+surname+" �������� � �������");
	}
	
	public static void giveTicket(){
		Student s=students.poll();
		System.out.println(s.name+" "+s.patronymic+" "+s.surname+" ������� ����� � ����� ������!");
	}
	
	public static void main(String[] args) {
		addStudent("�����", "������", "������", "���", "����");
		addStudent("�����", "������", "������", "���", "����");
		giveTicket();
		giveTicket();
	}

}
