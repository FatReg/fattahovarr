package Task14;

abstract class Plant {
	abstract void grow();
	abstract void watering();
	abstract void fertilizing();
	abstract void toBearFruit();
}
