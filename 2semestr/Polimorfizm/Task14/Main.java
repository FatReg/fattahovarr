package Task14;

public class Main {

	public static void main(String[] args) {
		Plant[] plant=new Plant[5];
		plant[0]=new Apple();
		plant[1]=new Plum();
		plant[2]=new Pear();
		plant[3]=new Currant();
		plant[4]=new Cherry();
		for(int i=0;i<plant.length;i++){
			plant[i].watering();
			plant[i].grow();
		}
	}

}
