package Task13;

public class Main {

	public static void main(String[] args) {
		
		Vehicle[] vehicle=new Vehicle[300];
		
		for(int i=0;i<50;i++){
			vehicle[i]=new Tram();
		}
		for(int i=50;i<100;i++){
			vehicle[i]=new Trolleybus();
		}
		for(int i=100;i<150;i++){
			vehicle[i]=new Bus();
		}
		for(int i=150;i<200;i++){
			vehicle[i]=new Car();
		}
		for(int i=200;i<250;i++){
			vehicle[i]=new Bicycle();
		}
		for(int i=250;i<300;i++){
			vehicle[i]=new Motorcycle();
		}
		
		for(int i=0;i<vehicle.length;i++)
			vehicle[i].drive();
	}
}
