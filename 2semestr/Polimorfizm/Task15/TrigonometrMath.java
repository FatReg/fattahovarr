package Task15;

public class TrigonometrMath {
	
	public static int factorial(int n)
    {
        if (n == 0) return 1;
        return n*factorial(n-1);
    }

	
	static double sin(int x){
		double sum=0;
		for(int n=1;n<15;n++){
			sum=sum+Math.pow(-1,n-1)*Math.pow(x,2*n-1)/factorial(2*n-1);
		}
		return sum;
	}
	
	static double cos(int x){
		double sum=0;
		for(int n=0;n<15;n++){
			sum=sum+Math.pow(-1,n)*Math.pow(x,2*n)/factorial(2*n);
		}
		return sum;
	}
	
	static double tg(int x){
		return sin(x)/cos(x);
	}
	
	static double ctg(int x){
		return cos(x)/sin(x);
	}
	
	static double e(int x){
		double sum=0;
		for(int n=0;n<15;n++){
			sum=sum+Math.pow(x,n)/factorial(n);
		}
		return sum;
	}
	
	static double arcsin(int x){
		double sum=x;
		for(int n=1;n<15;n++){
			sum=sum+factorial(factorial(2*n-1))*Math.pow(x, 2*n-1)/(factorial(2*n)*(2*n+1));
		}
		return sum;
	}
	
	static double arctg(int x){
		double sum=0;
		for(int n=0;n<15;n++){
			sum=sum+Math.pow(-1, n)*Math.pow(x, 2*n+1)/(2*n+1);
		}
		return sum;
	}
}
