public class AVL {

	public class Node {
		int key;
		int height;
		Node left;
		Node right;
	}

	private Node root = null;

	private Node newNode(int key) {
		Node node = new Node();
		node.key = key;
		node.height = 1;
		return node;
	}

	private int bfactor(Node node) {
		return height(node.right) - height(node.left);
	}

	private int height(Node node) {
		if (node != null)
			return node.height;
		else
			return 0;
	}

	private void fixheight(Node node) {
		int hl = height(node.left);
		int hr = height(node.right);
		if (hl > hr)
			node.height = hl + 1;
		else
			node.height = hr + 1;
	}

	private Node rotateright(Node p) {
		Node q = p.left;
		p.left = q.right;
		q.right = p;
		fixheight(p);
		fixheight(q);
		if (p == root)
			root = q;
		return q;
	}

	private Node rotateleft(Node p) {
		Node q = p.right;
		p.right = q.left;
		q.left = p;
		fixheight(p);
		fixheight(q);
		if (root == p)
			root = q;
		return q;
	}

	private Node balance(Node node) {
		fixheight(node);
		if (bfactor(node) == 2) {
			if (bfactor(node.right) < 0)
				node.right = rotateright(node.right);
			return rotateleft(node);
		} else if (bfactor(node) == -2) {
			if (bfactor(node.left) > 0)
				node.left = rotateleft(node.left);
			return rotateright(node);
		} else
			return node;
	}

	private Node add(Node node, int key) {
		if (node == null)
			return newNode(key);
		else if (key < node.key)
			node.left = add(node.left, key);
		else
			node.right = add(node.right, key);
		return balance(node);
	}

	void addKey(int key) {
		if (root == null)
			root = newNode(key);
		else
			add(root, key);
	}

	private Node findmin(Node node) {
		while (node.left != null)
			node = node.left;
		return node;
	}

	private Node removemin(Node node) {
		if (node.left == null)
			return node.right;
		node.left = removemin(node.left);
		return balance(node);
	}

	private Node remove(Node node, int key) {
		if (node != null) {
			if (key < node.key)
				node.left = remove(node.left, key);
			else if (key > node.key)
				node.right = remove(node.right, key);
			else {
				if (node.right == null){
					if(node==root)
						root=node.left;
					return node.left;
				}
				else {
					Node min = findmin(node.right);
					min.right = removemin(node.right);
					min.left = node.left;
					if (node == root)
						root = min;
					return balance(min);
				}
			}
		}
		return balance(node);
	}

	void delKey(int key) {
		if (isExist(key))
			remove(root, key);
		else
			System.out.println("��� ������ �����");
	}

	boolean isExist(int number) {
		return findNumber(root, number);
	}

	private boolean findNumber(Node current, int number) {
		if (current == null) {
			return false;
		} else if (current.key == number) {
			return true;
		} else if (current.key > number) {
			return findNumber(current.left, number);
		} else {
			return findNumber(current.right, number);
		}
	}

	Node findNode(int key) {
		if (isExist(key))
			return findNode(root, key);
		else {
			System.out.println("��� ������ �����");
			return null;
		}
	}

	private Node findNode(Node current, int number) {
		if (current == null) {
			return null;
		} else if (current.key == number) {
			return current;
		} else if (current.key > number) {
			return findNode(current.left, number);
		} else {
			return findNode(current.right, number);
		}
	}

	void show() {
		LKP(root);
	}

	private void LKP(Node current) {
		if (current == null)
			return;
		LKP(current.left);
		System.out.print(current.key + " ");
		LKP(current.right);
	}
}
