import java.util.ArrayList;
import java.util.Collections;

public class List {

	public class Node {
		String name, singer;
		int time;
		Node next;
		Node prev;
	}

	private Node header = null;
	private Node end = null;

	void addNewSong(Song song) {
		Node node = new Node();
		node.name = song.name;
		node.singer = song.singer;
		node.time = song.time;
		if (header == null) {
			header = node;
			end = node;
			node.next = node;
			node.prev = node;
		} else {
			if (header == end) {
				header.next = node;
				node.prev = header;
				end = node;
			} else {
				end.next = node;
				node.prev = end;
				end = node;
			}
			end.next = header;
			header.prev = end;
		}
	}

	void addNewSong(String name, String singer, int time) {
		Node node = new Node();
		node.name = name;
		node.singer = singer;
		node.time = time;
		if (header == null) {
			header = node;
			end = node;
			node.next = node;
			node.prev = node;
		} else {
			if (header == end) {
				header.next = node;
				node.prev = header;
				end = node;
			} else {
				end.next = node;
				node.prev = end;
				end = node;
			}
			end.next = header;
			header.prev = end;
		}
	}

	void showList() {
		Node current = header;
		if (header == end)
			if (header == null)
				System.out.println("������ ��������������� ����");
			else {
				System.out.println("song: " + current.name);
				System.out.println("singer: " + current.singer);
				System.out.println();
			}
		else {
			while (current.next != header) {
				System.out.println("song: " + current.name);
				System.out.println("singer: " + current.singer);
				System.out.println();
				current = current.next;
			}
			System.out.println("song: " + current.name);
			System.out.println("singer: " + current.singer);
			System.out.println();
		}
	}

	void delSong(String name) {
		if (header == end)
			if (header == null)
				System.out.println("������ ��������������� ����");
			else if (header.name == name) {
				header = null;
				end = null;
			} else
				System.out.println("����� ����� � ������ ���");
		else {
			Node current = header;
			while (current.next != header && current.name != name)
				current = current.next;
			if (current.name != name)
				System.out.println("����� ����� � ������ ���");
			else if (current == header || current == end) {
				if (current == header)
					header = header.next;
				else
					end = end.prev;
				header.prev = end;
				end.next = header;
			} else {
				current.prev.next = current.next;
				current.next.prev = current.prev;
			}
		}
	}

	void listenSong() {
		if (header == null)
			System.out.println("������ ��������������� ����");
		else {
			header = header.next;
			end = end.next;
		}
	}

	void showLovely() {
		if (header == null)
			System.out.println("������ ��������������� ����");
		else {
			Node current = header;
			ArrayList<String> list = new ArrayList<String>();
			list.add(current.singer);
			int max = 1;
			String string = current.singer;

			while (current.next != header) {
				current = current.next;
				list.add(current.singer);
				if (Collections.frequency(list, current.singer) > max) {
					max = Collections.frequency(list, current.singer);
					string = current.singer;
				}

			}

			System.out
					.println("� ������ ��������������� ���� ����� ����������� ����� ����������� "
							+ string);
		}
	}

	void showMaxAndMin() {
		if (header == null)
			System.out.println("������ ��������������� ����");
		else {
			int max = header.time;
			String maxsong=header.name;
			int min=header.time;
			String minsong=header.name;
			Node current = header;
			while (current.next != header) {
				current = current.next;
				if (current.time > max){
					max = current.time;
					maxsong=current.name;
				}
				if(current.time<min){
					min=current.time;
					minsong=current.name;
				}
			}
			System.out.println("������������ �� ����������������� ����� "+maxsong);
			System.out.println("����������� �� ����������������� ����� "+minsong);
		}
	}

}
