package Task19;

public class Stack {
	public class Node {
		int number;
		Node next;
	}

	private Node header = null;

	void add(int number) {
		Node node = new Node();
		node.number = number;
		node.next = header;
		header = node;
	}

	int max() {
		if (header == null)
			return 0;
		else {
			Node temp = header;
			int max = temp.number;
			while (temp != null) {
				if (max < temp.number)
					max = temp.number;
				temp = temp.next;
			}
			return max;
		}
	}

	int min() {
		if (header == null)
			return 0;
		else {
			Node temp = header;
			int min = temp.number;
			while (temp != null) {
				if (min > temp.number)
					min = temp.number;
				temp = temp.next;
			}
			return min;
		}
	}
}
