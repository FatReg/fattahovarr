package Task20;

public class Queue {

	public class Node {
		int number;
		Node next;
	}

	private Node header = null;

	void add(int number) {
		Node node = new Node();
		node.number = number;
		node.next = null;
		if (header != null) {
			Node temp = header;
			while (temp.next != null)
				temp = temp.next;
			temp.next = node;
		} else {
			header = node;
		}
	}

	int count() {
		int k = 0;
		Node temp = header;
		while (temp != null) {
			if (temp.number % 2 != 0)
				k++;
			temp = temp.next;
		}
		return k;
	}
}