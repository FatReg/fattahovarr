package Task21;

public class Mylist {

	public class Node {
		String name, variety;
		int price;
		Node next;
	}

	private Node header = null;

	void addAtTheBegin(Vegetables v) {
		Node node = new Node();
		node.name = v.name;
		node.price = v.price;
		node.variety = v.variety;
		node.next = header;
		header = node;
	}

	void addAtTheEnd(Vegetables v) {
		Node node = new Node();
		node.name = v.name;
		node.price = v.price;
		node.variety = v.variety;
		node.next = null;
		Node temp = header;
		while (temp.next != null)
			temp = temp.next;
		temp.next = node;
	}

	Vegetables removeBegin() {
		if (header == null) {
			Vegetables v = new Vegetables();
			return v;
		} else {
			Vegetables v = new Vegetables(header.name, header.variety,
					header.price);
			header = header.next;
			return v;
		}
	}

	Vegetables removeEnd() {
		if (header == null) {
			Vegetables v = new Vegetables();
			return v;
		} else {
			Node temp = header;
			while (temp.next != null)
				temp = temp.next;
			Vegetables v = new Vegetables(temp.name, temp.variety, temp.price);
			return v;
		}
	}

	Vegetables findVegetable(int i) {
		boolean x = true;
		if (header == null) {
			try {
				throw new ArrayIndexOutOfBoundException();
			} catch (ArrayIndexOutOfBoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Vegetables v = new Vegetables();
			return v;
		} else {
			Node temp = header;
			for (int j = 1; j < i && x; j++)
				if (temp.next != null)
					temp = temp.next;
				else
					x = false;
			if (x) {
				Vegetables v = new Vegetables(temp.name, temp.variety,
						temp.price);
				return v;
			} else {
				try {
					throw new ArrayIndexOutOfBoundException();
				} catch (ArrayIndexOutOfBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Vegetables v = new Vegetables();
				return v;
			}
		}
	}

	void showName() {
		Node temp = header;
		while (temp != null) {
			System.out.println(temp.name);
			temp = temp.next;
		}
	}
}
