package Vector;

public class Vector {
	double mas[];
	
	public Vector(double mas[]){
		this.mas=mas;
	}
	
	private void newMas(double a[]){
		this.mas=a;
	}
	
	void Clean(){
		for(int i=0;i<mas.length;i++)
			mas[i]=0;
	}
	
	void AddEnd(double a){
		double m[]=new double[mas.length+1];
		for(int i=0;i<mas.length;i++)
			m[i]=mas[i];
		m[m.length-1]=a;
		newMas(m);
	}
	
	void AddEl(double a,int k){
		double m[]=new double[mas.length+1];
		for(int i=0;i<k-1;i++)
			m[i]=mas[i];
		m[k-1]=a;
		for(int i=k;i<m.length;i++)
			m[i]=mas[i-1];
		newMas(m);
	}
	
	void DelEl(int k){
		double m[]=new double[mas.length-1];
		for(int i=0;i<k-1;i++)
			m[i]=mas[i];
		for(int i=k-1;i<m.length;i++)
			m[i]=mas[i+1];
		newMas(m);
	}
	
	void Show(){
		System.out.println();
		for(int i=0;i<mas.length;i++)
			System.out.print(mas[i]+" ");
	}
}
