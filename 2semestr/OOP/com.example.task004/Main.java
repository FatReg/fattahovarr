package Vector;

public class Main {

	public static void main(String[] args) {
		double a[]=new double[]{1,2,3};
		Vector v=new Vector(a);
		v.Show();
		v.AddEnd(5);
		v.Show();
		v.AddEnd(6);
		v.Show();
		v.DelEl(3);
		v.Show();
		v.AddEl(1.2, 2);
		v.Show();
	}

}
