package Mail;

public class Mail {
	String country,region,city,street,house;
	int index;
	
	Mail(String country,String region,String city,String street,String house){
		this.country=country;
		this.region=region;
		this.city=city;
		this.street=street;
		this.house=house;
	}
	
	Mail(String country,String region,String city,String street){
		this.country=country;
		this.region=region;
		this.city=city;
		this.street=street;
	}
	
	Mail(String country,String region,String city){
		this.country=country;
		this.region=region;
		this.city=city;
	}
	
	Mail(String country,String region){
		this.country=country;
		this.region=region;
	}
	
	Mail(String country){
		this.country=country;
	}
	
	Mail(int i){
		this.index=i;
	}
	
	void setCountry(String country){
		this.country=country;
	}
	
	String getCountry(){
		return country;
	}
	
	void setRegion(String region){
		this.region=region;
	}
	
	String getRegion(){
		return region;
	}
	
	void setCity(String city){
		this.city=city;
	}
	
	String getCity(){
		return city;
	}
	
	void setStreet(String street){
		this.street=street;
	}
	
	String getStreet(){
		return street;
	}
	
	void setHouse(String house){
		this.house=house;
	}
	
	String getHouse(){
		return house;
	}
	
	void setIndex(int i){
		this.index=i;
	}
	
	int getIndex(){
		return index;
	}
	
	
}
