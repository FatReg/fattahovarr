package Matrix;

public class Main {

	public static void main(String[] args) {
		double a[][]=new double[][]{{1,1,1},{2,2,2}};
		double b[][]=new double[][]{{1,1},{2,2},{0,0}};
		Matrix m1=new Matrix(3,4);
		m1.Show();
		Matrix m=new Matrix(a);
		m.Show();
		m.MultMatrix(b);
		m.Show();
		m.MultNumber(2);
		m.Show();
		m.Addition(a);
		m.Show();
		m.Transposition();
		m.Show();
	}

}
