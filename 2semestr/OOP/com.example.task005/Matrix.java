package Matrix;

public class Matrix {
	double mas[][];
	
	public Matrix(double mas[][]){
		this.mas=mas;
	}
	
	private void newMatr(double a[][]){
		this.mas=a;
	}
	
	public Matrix(int i,int j){
		mas=new double[i][j];
	}
	
	void Addition(double mas[][]){
		if(mas.length!=this.mas.length||mas[0].length!=this.mas[0].length)
			System.out.println("addition Error");
		else
			for(int i=0;i<mas.length;i++)
				for(int j=0;j<mas[i].length;j++)
					this.mas[i][j]=this.mas[i][j]+mas[i][j];
	}
	
	void MultNumber(int k){
		for(int i=0;i<mas.length;i++)
			for(int j=0;j<mas[i].length;j++)
				mas[i][j]=mas[i][j]*k;
	}
	
	void MultMatrix(double mas[][]){
		if (this.mas[0].length!=mas.length)
			System.out.println("Matrix multiplication error");
		else{
			double m[][]=new double[this.mas.length][mas[0].length];
			for(int i=0;i<m.length;i++)
				for(int j=0;j<m[i].length;j++)
					for (int k=0;k<mas.length;k++)
	                    m[i][j]=m[i][j]+this.mas[i][k]*mas[k][j]; 
			newMatr(m);
		}	
	}
	
	void Transposition(){
		double m[][]=new double[mas[0].length][mas.length];
		for(int i=0;i<m.length;i++)
			for(int j=0;j<mas[0].length;j++)
				m[i][j]=mas[j][i];
		newMatr(m);
	}
	
	void Show(){
		System.out.println();
		for(int i=0;i<mas.length;i++){
			System.out.println();
			for(int j=0;j<mas[i].length;j++)
				System.out.print(mas[i][j]+" ");
		}
	}
}
