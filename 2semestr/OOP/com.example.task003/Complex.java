package Complex;

public class Complex {
	double Z, Im;
	
	Complex(double z,double im){
		Z=z;
		Im=im;
	}
	
	void setZ(double z){
		Z=z;
	}
	
	void setIm(double im){
		Im=im;
	}
	
	double getZ(){
		return Z;
	}
	
	double getIm(){
		return Im;
	}
	
	double Module(){
		return Math.sqrt(Z*Z+Im*Im);
	}
	
	String Addition(double a,double b){
		if((Im+b)>=0)
			return (Z+a)+"+"+(Im+b)+"i";
		else
			return (Z+a)+" "+(Im+b)+"i";
	}
	
	String Multiplication(double a,double b){
		if((Z*b+Im*a)>=0)
			return (Z*a-Im*b)+"+"+(Z*b+Im*a)+"i";
		else
			return (Z*a-Im*b)+" "+(Z*b+Im*a)+"i";
	}
	
	String getComplex(){
		if(Im>=0)
			return Z+"+"+Im+"i";
		else
			return Z+""+Im+"i";
	}
}
