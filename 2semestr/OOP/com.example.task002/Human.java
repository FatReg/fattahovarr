package Human;

public class Human {

	private String name, gender;
	private int age;
	private double weight;
	
	public Human(String name){
		this.name=name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public void setAge(int age){
		this.age=age;
	}
	
	public void setGender(String gender){
		this.gender=gender;
	}
	
	public void setWeight(double weight){
		this.weight=weight;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGender(){
		return gender;
	}
	
	public int getAge(){
		return age;
	}
	
	public double getWeight(){
		return weight;
	}
}
