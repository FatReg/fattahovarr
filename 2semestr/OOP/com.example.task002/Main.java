package Human;

public class Main {

	public static void main(String[] args) {
		Human human = new Human("Barny");
		human.setGender("male");
		human.setAge(3);
		human.setWeight(5.6);
		System.out.println(human.getName());
		System.out.println(human.getGender());
		System.out.println(human.getAge());
		System.out.println(human.getWeight());

	}

}
