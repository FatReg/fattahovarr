import java.util.Scanner;


public class Task009 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		int n,max,min,x;
		System.out.println("Vvedite chislo");
		n=sc.nextInt();
		sc.close();
		max=0;
		min=9;
		for(int i=n;i>0;i=n){
			x=n%10;
			if(x<min)
				min=x;
			if(x>max)
				max=x;
			n=n/10;
		}
		x=max-min;
		if(x%2==0)
			System.out.print(x+" chetnoe");
		else
			System.out.print(x+" nechetnoe");
	}

}
