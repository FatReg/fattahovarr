/*62. ���� ������ ���� � �������� �������������� ����������. �������� ����������� ������� ��� ���������� n-�� ����� ���������� ����� n ������ ������ ����������.
��������: sumProg
���������: ��� � 1-� ���� ����������.
������������ ��������: int
*/
public class Task062 {
	
	public static int findMembofProg(int a,int d,int n){
		if (n==1)
			return a;
		else
			return d+findMembofProg(a, d, n-1);
	}
	
	public static int sumProg(int a,int d,int n){
		int sum=findMembofProg(a, d, n);
		if(n==1)
			return sum;
		else
			return sum+sumProg(a,d,n-1);
	}
	
	public static void main(String[] args) {
		int a=1,d=3,n=4;
		System.out.println(sumProg(a,d,n));

	}

}