/* �������� ����������� ��������� ��� ������ �� ����� ���� ������������ ����� � �������� �������.
��������: reverseNumber
���������: ����� ���� int
������������ ��������: ���
*/
public class Task064 {
	
	public static void reverseNumber(int a){
		if(a/10<1)
			System.out.print(a);
		else{
			System.out.print(a%10);
			reverseNumber(a/10);
		}
	}
	
	public static void main(String[] args) {
		int a=123456;
		reverseNumber(a);
	}

}
