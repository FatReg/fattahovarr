package Task4;

public class Metro {
	private int cars[],MAX_PEOPLE,MAX_STOP,STOP;
	
	public Metro(int mas[],int k,int l){
		MAX_STOP=k;
		cars=mas;
		MAX_PEOPLE=l;
	}
	
	public void enterPoeple(int mas[]){ 
		for(int i=0;i<mas.length;i++){
			if(cars[i]+mas[i]>MAX_PEOPLE){
				System.out.println("Sorry "+(i+1)+"-i vagon full");
				cars[i]=MAX_PEOPLE;
			}
			else{
				cars[i]=cars[i]+mas[i];
				System.out.println("V vagone "+(i+1)+" edet "+cars[i]+" ludei");
			}
		}
	}
	
	public void goOutPeople(int mas[]){
		for(int i=0;i<mas.length;i++){
			if(cars[i]<mas[i]){
				cars[i]=0;
				System.out.println("V vagone "+(i+1)+" edet "+cars[i]+" ludei");
			}
			else{
				cars[i]=cars[i]-mas[i];
				System.out.println("V vagone "+(i+1)+" edet "+cars[i]+" ludei");
		}
		}
	}
	
	public void increaseStop(){
		STOP++;
	}
	
	public void decreaseStop(){
		STOP--;
	}
	
	public void run(){
		STOP=0;
		while(STOP!=MAX_STOP)
			increaseStop();
	}

}
