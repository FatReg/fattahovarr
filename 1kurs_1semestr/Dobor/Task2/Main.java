package Task2;

public class Main {
	
	public static int Nod(int a, int b) {
    	while(a!=0&&b!=0)
    	    if(a>b)
    	        a=a%b;
    	    else
    	        b=b%a;
    	int nod=a+b;
    	return nod;
	}
	
	public static int NodStr(int mas[]){
		int nod=Nod(mas[1],mas[0]);
		for(int i=2;i<mas.length;i++){
			nod=Nod(nod,mas[i]);
		}
		return nod;
	}
	
	public static void NodMatr(int a[][]){
		int mas[]=new int[a[0].length];
		if(NodStr(a[0])<NodStr(a[1])){
			mas=a[0];
			a[0]=a[1];
			a[1]=mas;
		}
			
	}
	
	public static void main(String[] args) {
		int matr[][];
		matr=new int[][]{{3,6,6},{27,18,9}};
		NodMatr(matr);
		for(int i=0;i<matr.length;i++){
			for(int j=0;j<matr[i].length;j++)
				System.out.print(matr[i][j]+" ");
			System.out.println();
		}
	}

}
