package Clock;

public class Clock {

	int sec,min,hour;
	
	public Clock(int s,int m,int h){
		sec=s;
		min=m;
		hour=h;
	}
	
	public Clock(){
		sec=0;
		min=0;
		hour=0;
	}
	
	public void increaseSec(){
		if(sec!=60)
			sec++;
		else{
			sec=0;
			increaseMin();
		}
	}
	
	public void increaseMin(){
		if(min!=60)
			min++;
		else{
			min=0;
			increaseHour();
		}
	}
	
	public void increaseHour(){
		if(hour!=24)
			hour++;
		else
			hour=0;
	}
	
	public String getTime(){
		return (hour+":"+min+":"+sec);
	}
	
	public void Work(){
		System.out.println(getTime());
		int k=hour,l=min,m=sec;
		while(k==hour&&l!=min&&m!=sec)
			increaseSec();
		System.out.println(getTime());
	}

}
