package Calculate;

public class Calculate {
	double op1,op2;
	char operator;
	
	public Calculate(double op1, double op2, char operator){
		this.op1=op1;
		this.op2=op2;
		this.operator=operator;
	}
	
	public double Sum(){
		return op1+op2;
	}
	
	public double Mult(){
		return op1*op2;
	}
	
	public double Div(){
		return (double)(op1)/(op2);
	}
	
	public double Minus(){
		return op1-op2;
	}
	
	public void getResult(){
		if(operator=='+')
			System.out.println(Sum());
		if(operator=='-')
			System.out.println(Minus());
		if(operator=='/')
			System.out.println(Div());
		if(operator=='*')
			System.out.println(Mult());
	}

}
