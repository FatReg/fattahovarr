package Svetophor;

public class Svetophor {
	
	String color;
	
	public Svetophor(String color){
		this.color=color;
	}
	
	public void switchToRed(){
			color="Red";
			System.out.println("Now color is "+color);
	}
	
	public void switchToYellow(){
			color="Yellow";
			System.out.println("Now color is "+color);
	}
	
	public void switchToGreen(){
			color="Green";
			System.out.println("Now color is "+color);
	}
	
	public void Work(){
		int k=7;
		boolean x=true;
		while(k>0){
			if(color=="Yellow")
				if(x)
					switchToRed();
				else
					switchToGreen();
			else{
				if(color=="Red")
					x=false;
				else
					x=true;
				switchToYellow();
			}
			k--;
		}
	}
}
