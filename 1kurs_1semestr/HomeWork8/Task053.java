/*���� ����������� ����� n � ����� ����� a1, a2, ..., an. ����� ���������� ����� ai (i = 1, 2, ..., n), 
 * ���������� ��������� �������. ���������� �������, ����������� ������������ ������� �������.
��������: isFivePow
���������: x ���� int
������������ ��������: boolean
*/
public class Task053 {
	public static boolean isFivePow(int x){
		boolean a=false;
		if(x%5==0)
			a=true;
		return a;
	}
	public static void main(String[] args) {
		int n=5;
		int mas[]=new int[n];
		for(int i=0;i<mas.length;i++){
			mas[i]=(int)(Math.random()*100);
			if(isFivePow(mas[i]))
				System.out.println(mas[i]+" Stepen' 5-ki");
		}
	}

}
