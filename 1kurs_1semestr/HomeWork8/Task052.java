/*�������� �������, ������� ����� ��������� �������� true, ���� x = 1, y = 0 � z= 1.
 *  ������ ����� ����������. ������������ ������ �������
��������:  isRight
���������: x, y, z ���� boolean
������������ ��������:  boolean
*/
public class Task052 {
	public static boolean isRight(boolean a,boolean b,boolean c){
		boolean	x=a^!b^c;
		return x;
	}
	public static void main(String[] args) {
		boolean x=true,y=false,z=true;
		if(isRight(x, y, z))
			System.out.println("true");
		else
			System.out.println("false");
	}

}
