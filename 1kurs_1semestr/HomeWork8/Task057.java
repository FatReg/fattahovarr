/*57. �������� �������, ������� �� ��������� ������ ������ � ���� ���������� ���������� ����. 
 * ������� ������ ��������� ���������� ����.
 * ��������: countDay
 * ������� ���������: int month, int year.
 * ������������ ��������: int
*/
public class Task057 {
	public static int countDay(int month, int year){
		int kol=0;
		boolean x=false;
		if(year%400==0)
			x=true;
		else
			if(year%100!=0&&year%4==0)
				x=true;
		if(month==1||month==3||month==5||month==7||month==8||month==10||month==12)
			kol=31;
		else
			if(month==4||month==6||month==9||month==11)
				kol=30;
			else
				if(x)
					kol=29;
				else
					kol=28;
		return kol;
	}
	public static void main(String[] args) {
		int num_month=2,year=2008;
		System.out.println("Kol-vo dney= "+countDay(num_month, year));
	}

}
