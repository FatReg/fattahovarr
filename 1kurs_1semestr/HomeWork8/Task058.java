/* ����� ���������� ����� ��������(���) ���� ����������� �����, ���� � ����, ��� ���(a, b, c) = ���(���(a, b), c).
 * ���������� ������� ��� ������� ����������� ������ �������� ���� ����������� �����.
��������: Nod
���������: a, b ���� int
������������ ��������: int
*/
public class Task058 {
    public static int Nod(int a, int b) {
    	while(a!=0&&b!=0)
    	    if(a>b)
    	        a=a%b;
    	    else
    	        b=b%a;
    	int nod=a+b;
    	return nod;
    }
    public static void main(String[] args) {
    		int a=27,b=6,c=54;
	        System.out.println("NOD="+Nod(Nod(a,b),c));
	    }
}
