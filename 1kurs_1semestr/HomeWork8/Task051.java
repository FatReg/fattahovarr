/*�������� �������, ������� ����� ����������, �������� �� ����� ���� ������� ����� ������ ������
��������: isParity
���������: ���������� ���� int
������������ ��������:  boolean
*/
public class Task051 {
	public static boolean isParity(int a){
		boolean x=true;
		int sum=0;
		while(a>0){
			sum=sum+a%10;
			a=a/10;
		}
		if(sum%2!=0)
			x=false;
		return x;
	}
	public static void main(String[] args) {
		int chislo=12345;
		if(isParity(chislo))
			System.out.println("sum zifr chetna");
		else
			System.out.println("sum zifr nechetna");
	}

}
