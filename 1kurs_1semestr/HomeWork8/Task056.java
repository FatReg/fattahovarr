/*�������� �������, ������� ����� ���������� �������� �� ��������� ��� ����������. 
��������: isLeapYear
���������: year ���� int
������������ ��������: boolean
*/
public class Task056 {
	public static boolean isLeapYear(int year){
		boolean x=false;
		if(year%400==0)
			x=true;
		else
			if(year%100!=0&&year%4==0)
				x=true;
		return x;
	}
	public static void main(String[] args) {
		int year=2008;
		if(isLeapYear(year))
			System.out.println(year+" god yav-sya visokosnim");
		else
			System.out.println(year+" god ne yav-sya visokosnim");
	}

}
