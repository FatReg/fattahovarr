/*��������� ����� ����� ������������ � ���� ����������� �����������
������� �������� 8�8. ���� ����������� ����� v � g (1 v 8, 1 g 8),
����������� ������ ��������� � ����������� ��� ����, �� ������� �����
��������� ������. ���������� ����, ����������� ��� ������� ������ ������,
�������� ������� ������� "*", � ��������� � ������� "0". ���-
�������� ������, ����� �������� ������:
�) �����; 	�) �����;
�) ����; 	�) ����.
*/
public class Task039 {
	public static void main(String[] args) {
		int v,g,k;
		int a[][]=new int[8][8];
		v=6;
		g=2;
		System.out.println("Ladya:");
		for(int i=0;i<8;i++){
			System.out.println();
			for(int j=0;j<8;j++)
				if((i==g-1)||(j==v-1))
					System.out.print("* ");
				else
					System.out.print(a[i][j]+" ");
			
		}
		System.out.println();
		System.out.println();
		System.out.println("Slon:");
		k=v-1;
		for(int i=g-1;i>=0&&k>=0;i--){
			a[i][k]=1;
			k--;
		}
		k=v-1;
		for(int i=g-1;i>=0&&k<8;i--){
			a[i][k]=1;
			k++;
		}
		k=v-1;
		for(int i=g-1;i<8&&k>=0;i++){
			a[i][k]=1;
			k--;
		}
		k=v-1;
		for(int i=g-1;i<8&&k<8;i++){
			a[i][k]=1;
			k++;
		}
		for(int i=0;i<8;i++){
			System.out.println();
			for(int j=0;j<8;j++)
				if(a[i][j]==1)
					System.out.print("* ");
				else
					System.out.print(a[i][j]+" ");
		}
		System.out.println();
		System.out.println();
		System.out.println("Ferz':");
		for(int i=0;i<8;i++){
			System.out.println();
			for(int j=0;j<8;j++)
				if(a[i][j]==1||(i==g-1)||(j==v-1)){
					System.out.print("* ");
					a[i][j]=0;
				}
				else
					System.out.print(a[i][j]+" ");
		}
		System.out.println();
		System.out.println();
		System.out.println("Kon':");
		for(int i=0;i<8;i++){
			System.out.println();
			for(int j=0;j<8;j++)
				if(((i==g-2)||(i==g))&&((j==v+1)||(j==v-3))||((i==g+1)||(i==g-3))&&((j==v)||(j==v-2)))
					System.out.print("* ");
				else
					System.out.print(a[i][j]+" ");
			
		}
	}

}
